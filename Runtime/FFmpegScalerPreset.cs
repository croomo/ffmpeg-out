﻿namespace FFmpegOut
{
    public enum FFmpegScalerPreset
    {
        FastBilinear,
        Bilinear,
        Bicubic,
        Experimental,
        Neighbor,
        Area,
        Bicublin,
        Gauss,
        Sinc,
        Lanczos,
        Spline
    }

    static public class FFmpegScalerPresetExtensions
    {
        public static string GetDisplayName(this FFmpegScalerPreset preset)
        {
            switch (preset)
            {
                case FFmpegScalerPreset.FastBilinear: return "H.264 Default (MP4)";
                case FFmpegScalerPreset.Bilinear: return "H.264 NVIDIA (MP4)";
                case FFmpegScalerPreset.Bicubic: return "H.264 Lossless 420 (MP4)";
                case FFmpegScalerPreset.Experimental: return "H.264 Lossless 444 (MP4)";
                case FFmpegScalerPreset.Neighbor: return "HEVC Default (MP4)";
                case FFmpegScalerPreset.Area: return "HEVC NVIDIA (MP4)";
                case FFmpegScalerPreset.Bicublin: return "ProRes 422 (QuickTime)";
                case FFmpegScalerPreset.Gauss: return "ProRes 4444 (QuickTime)";
                case FFmpegScalerPreset.Sinc: return "VP8 (WebM)";
                case FFmpegScalerPreset.Lanczos: return "VP9 (WebM)";
                case FFmpegScalerPreset.Spline: return "HAP (QuickTime)";
            }
            return null;
        }

        public static string GetOptions(this FFmpegScalerPreset preset)
        {
            switch (preset)
            {
                case FFmpegScalerPreset.FastBilinear: return "-sws_flags fast_bilinear";
                case FFmpegScalerPreset.Bilinear: return "-sws_flags bilinear";
                case FFmpegScalerPreset.Bicubic: return "-sws_flags bicubic";
                case FFmpegScalerPreset.Experimental: return "-sws_flags experimental";
                case FFmpegScalerPreset.Neighbor: return "-sws_flags neighbor";
                case FFmpegScalerPreset.Area: return "-sws_flags area";
                case FFmpegScalerPreset.Bicublin: return "-sws_flags bicublin";
                case FFmpegScalerPreset.Gauss: return "-sws_flags gauss";
                case FFmpegScalerPreset.Sinc: return "-sws_flags sinc";
                case FFmpegScalerPreset.Lanczos: return "-sws_flags lanczos";
                case FFmpegScalerPreset.Spline: return "-sws_flags spline";
            }
            return null;
        }
    }
}
